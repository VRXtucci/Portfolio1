import Hero from './Components/Hero'
import Navbar from './Components/Navbar'
import Projects from './Components/Projects'
import Skills from './Components/skills'
import Footer from './Components/Footer'
import './index.css'

function App() {
  return (
    <>
        <Navbar />
      <main className=' py-20'>
        <Hero />
        <Skills/>
        <Projects/>
      </main>
      <Footer/>
    </>
  )
}

export default App
