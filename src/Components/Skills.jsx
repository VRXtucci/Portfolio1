import CSS3 from '../assets/css3.svg'
import HTML5 from '../assets/html5.svg'
import JS from '../assets/javascript.svg'
import React from '../assets/react.svg'
import Tailwind from '../assets/tailwind.svg'
import Git from '../assets/git.svg'
import Axios from '../assets/Axios.svg'
import GitHub from '../assets/github.svg'

const Skills = () => {
    return (
        <>
            <section id='Skills' className='px-72 gap-10 grid bg-BlueBayoux-800 py-12 h-screen'>
                <h2 className=" font-BlackOpsOne text-Viking-500 font-normal text-4xl uppercase">
                    <span className=' text-PictonBlue-300'>
                        &lt;
                    </span>
                    &nbsp;Habilidades&nbsp;
                    <span className=' text-PictonBlue-300'>
                        /&gt;
                    </span>
                </h2>
                <div className='flex justify-between'>
                    <article>
                        <h3 className=' font-BebasNeue text-white text-3xl'>Html</h3>
                        <img src={HTML5} alt="html" />

                    </article>
                    <article>
                        <h3 className=' font-BebasNeue text-white text-3xl'>Css</h3>
                        <img src={CSS3} alt="css" />

                    </article>
                    <article>
                        <h3 className=' font-BebasNeue text-white text-3xl'>JavaScript</h3>
                        <img src={JS} alt="javascript" />

                    </article>
                </div>
                <div className='flex justify-between'>
                    <article>
                        <h3 className=' font-BebasNeue text-white text-3xl'>React</h3>
                        <img src={React} alt="React" />

                    </article>
                    <article>
                        <h3 className=' font-BebasNeue text-white text-3xl'>Axios</h3>
                        <img src={Axios} alt="Axios" width="128px" />

                    </article>
                    <article>
                        <h3 className=' font-BebasNeue text-white text-3xl'>Tailwind</h3>
                        <img src={Tailwind} alt="Tailwind" />

                    </article>
                </div>
                <div className='flex justify-evenly'>
                    <article>
                        <h3 className=' font-BebasNeue text-white text-3xl'>Git</h3>
                        <img src={Git} alt="Git" />
                    </article>
                    <article>
                        <h3 className=' font-BebasNeue text-white text-3xl'>GitHub</h3>
                        <img src={GitHub} alt="GitHub" />
                    </article>
                </div>
            </section>
        </>
    );
};

export default Skills;
