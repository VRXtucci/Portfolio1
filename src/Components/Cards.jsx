import React from 'react';

const Cards = ({ title, imageUrl, description, technologies, pageUrl, codeUrl }) => {
  return (
    <div className='w-[35rem] bg-PictonBlue-500 p-4 my-6 rounded-xl'>
      <h3 className='font-BebasNeue text-white text-3xl py-4'>{title}</h3>
      <div className='flex justify-center'>
        <img src={imageUrl} alt="IMG" className='w-[30rem] rounded-xl' />
      </div>
      <p className='font-BebasNeue text-white text-xl p-4'>{description}</p>
      <div>
        <h4 className='font-BebasNeue text-white text-2xl'>Technologies:</h4>
        <div className='flex items-center justify-between'>
          <ul className='flex gap-4 py-4 px-4'>
            {technologies.map((tech, index) => (
              <li key={index}>
                <img src={tech.icon} alt={tech.name} className='w-16' />
              </li>
            ))}
          </ul>
          <ul className='flex gap-4'>
            <li>
              <a href={pageUrl} target="_blank" rel="noopener noreferrer" className='font-BebasNeue text-white text-2xl hover:text-Viking-800'>
                Pagina
              </a>
            </li>
            <li>
              <a href={codeUrl} target="_blank" rel="noopener noreferrer" className='font-BebasNeue text-white text-2xl hover:text-Viking-800'>
                Codigo
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Cards;
