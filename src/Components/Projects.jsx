import Cards from './Cards';
import PokeApp from '../assets/PokeApp.png'
import TodoApp from '../assets/TO-DOApp.png'
import Windbnd from '../assets/Windbnd.png'
import Insure from '../assets/Insure.png'
import MyTeamPage from '../assets/MyTeamPage.png'
import CSS3 from '../assets/css3.svg'
import HTML5 from '../assets/html5.svg'
import JS from '../assets/javascript.svg'
import React from '../assets/react.svg'
import Tailwind from '../assets/tailwind.svg'
import Git from '../assets/git.svg'
import Axios from '../assets/Axios.svg'
import GitHub from '../assets/github.svg'

const Projects = () => {
    return (
        <>
            <section id='Projects' className="mx-72 my-10">
                <h2 className="font-BlackOpsOne text-Viking-500 font-normal text-4xl uppercase">
                    <span className='text-PictonBlue-300'>&lt;</span>
                    &nbsp;Proyectos&nbsp;
                    <span className='text-PictonBlue-300'>&gt;</span>
                </h2>
                <div className=' grid grid-cols-2'>
                    <Cards
                        title="TO-DO App"
                        imageUrl={TodoApp}
                        description="Proyecto de Curso de programacion de un recordatorio de tareas"
                        technologies={[
                            { name: 'Html', icon: HTML5 },
                            { name: 'Css', icon: CSS3 },
                            { name: 'javascript', icon: JS },
                        ]}
                        pageUrl="https://proyectofinalfunval.netlify.app"
                        codeUrl="https://github.com/VRXtucci/To-Do-App"
                    />
                    <Cards
                        title="Insuren"
                        imageUrl={Insure}
                        description="Proyecto de Curso de programacion de la utilizacion de HTML y CSS"
                        technologies={[
                            { name: 'Html', icon: HTML5 },
                            { name: 'Css', icon: CSS3 },
                        ]}
                        pageUrl="https://mini-poryecto.netlify.app"
                        codeUrl="https://github.com/VRXtucci/Insure-Landing"
                    />
                    <Cards
                        title="Pokedex App"
                        imageUrl={PokeApp}
                        description="Proyecto Cargando la Api de pokemon utilizando react, axios y javascript."
                        technologies={[
                            { name: 'React', icon: React },
                            { name: 'Axios', icon: Axios },
                            { name: 'javascript', icon: JS },
                        ]}
                        pageUrl="https://pokedex-app-tucci.netlify.app"
                        codeUrl="https://github.com/VRXtucci/PokedexApp"
                    />
                    <Cards
                        title="Windbnb"
                        imageUrl={Windbnd}
                        description="Proyecto Cargando Api de Hoteles de Finlandia utilizando react, axios y javascript."
                        technologies={[
                            { name: 'React', icon: React },
                            { name: 'Axios', icon: Axios },
                            { name: 'javascript', icon: JS },
                        ]}
                        pageUrl="https://windbnbtucci.netlify.app"
                        codeUrl="https://github.com/VRXtucci/windbnbtucci"
                    />
                    <Cards
                        title="The creative crew"
                        imageUrl={MyTeamPage}
                        description="Proyecto realizado con Tailwind, HTML y Javascript "
                        technologies={[
                            { name: 'Tailwind', icon: Tailwind },
                            { name: 'Html', icon: HTML5 },
                            { name: 'javascript', icon: JS },
                        ]}
                        pageUrl="https://darkmodetailwind.netlify.app"
                        codeUrl="https://github.com/VRXtucci/MyTeamPage"
                    />
                </div>
            </section>
        </>
    );
};

export default Projects;
