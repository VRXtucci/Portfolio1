import { Link } from 'react-scroll';

const Navbar = () => {
  return (
    <>
      <nav id="navbar" className=" fixed top-0 bg-Viking-900 flex items-center justify-around w-full h-20 text-white px-4 z-50">
        <div className="p-3">
          <h2 className="hover:text-BlueBayoux-400 font-BlackOpsOne text-PictonBlue-600 font-normal text-5xl uppercase">tucci</h2>
        </div>
        <div>
          <ul className="hidden md:flex items-center align-middle justify-center font-BebasNeue">
            <Link to="Hero" smooth={true} duration={500} className=" hover:text-BlueBayoux-400 text-2xl px-3 cursor-pointer duration-200">
              &lt; inicio /&gt;
            </Link>
            <Link to="Skills" smooth={true} duration={500} className=" hover:text-BlueBayoux-400 text-2xl px-3 cursor-pointer duration-200">
              &lt; Habilidades /&gt;
            </Link>
            <Link to="Projects" smooth={true} duration={500} className=" hover:text-BlueBayoux-400 text-2xl px-3 cursor-pointer duration-200">
              &lt; proyectos /&gt;
            </Link>
          </ul>
        </div>
        <div className="cursor-pointer pr-4 z-10 md:hidden ">
          <svg stroke="currentColor" fill="currentColor" strokeWidth="0" viewBox="0 0 448 512" className="dark:text-black" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path>
          </svg>
        </div>
      </nav>
    </>

  );
};

export default Navbar;
