// Hero.jsx

import React, { useState, useEffect } from 'react';
import Profile from '../assets/Profile.jpg';
import Github from '../assets/github.png';
import Linkedin from '../assets/linkedin.png';
import '../index.css';

const phrases = [
    "¡Amante de los Videojuegos!",
    "Desarrollador Frontend",
    "Apasionado por la tecnología"
    // Agrega más frases según sea necesario
];

const Hero = () => {
    const [phraseIndex, setPhraseIndex] = useState(0);
    const [isTyping, setIsTyping] = useState(true);

    useEffect(() => {
        const timer = setTimeout(() => {
            setIsTyping((prev) => !prev);
            if (!isTyping) {
                // Cambia a la siguiente frase después de que la animación de escritura inversa ha terminado
                setPhraseIndex((prevIndex) => (prevIndex + 1) % phrases.length);
            }
        }, 3000); // Cambia el valor según la duración de la animación original

        return () => clearTimeout(timer);
    }, [isTyping, phraseIndex]);

    return (
        <>
            <div id='Hero'>
                <header className='py-10 px-72 flex items-center'>
                    <div>
                        <img src={Profile} alt='Profile' className='w-60 rounded-full shadow-2xl' />
                        <ul className='flex justify-center gap-10 py-5'>
                            <li>
                                <a href="https://github.com/VRXtucci">
                                    <img src={Github} alt="Github" className=' w-10' />
                                </a>
                            </li>
                            <li>
                                <a href="linkedin.com/in/jose-tucci-7bb87918a/">
                                    <img src={Linkedin} alt="Linkedin" className=' w-10' />
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className='flex flex-col items-center'>
                        <h2 className='font-BlackOpsOne font-normal uppercase text-8xl mx-24 text-Viking-500'>
                            Jose Tucci.
                            <span className=' font-BebasNeue lowercase block text-white'>
                                Frontend Developer.
                            </span>
                        </h2>
                        <div className='typing-container'>
                            <p className={`font-BebasNeue text-2xl text-white tracking-[.5rem] ${isTyping ? 'typing-effect' : 'typing-reverse'}`}>
                                {phrases[phraseIndex]}
                            </p>
                        </div>
                    </div>
                </header>
                <main className='px-72'>
                    <h2 className='font-BlackOpsOne font-normal uppercase text-4xl text-Viking-500'>
                        <span className=' text-PictonBlue-300'>
                            &lt;
                        </span>
                        &nbsp;sobre mí&nbsp;
                        <span className=' text-PictonBlue-300'>
                            /&gt;
                        </span>
                    </h2>
                    <div className=' w-[45rem]'>
                        <p className='font-BebasNeue text-white font-normal text-2xl py-1'>
                            ¡Sean bienvenidos! Soy José Tucci, tengo actualmente 24 años y me desempeño como programador frontend.
                            Desde los 12 años, he sentido una fuerte afinidad por el mundo de la informática y los videojuegos.
                            Me considero una persona sociable, amigable y responsable.
                        </p>
                        <p className='font-BebasNeue text-white font-normal text-2xl py-5'>
                            A partir de los 17 años, me adentré en la programación web y el diseño de interfaces.
                            Hace aproximadamente un año, concluí con éxito un curso de programación web full stack,
                            enfocándome especialmente en el desarrollo del diseño web y la interfaz con el usuario.
                        </p>
                    </div>
                </main>
            </div>
        </>
    );
};

export default Hero;
