import React, { useState } from 'react';

const Footer = () => {
    const [name, setName] = useState('');
    const [message, setMessage] = useState('');

    const handleNameChange = (e) => {
        setName(e.target.value);
    };

    const handleMessageChange = (e) => {
        setMessage(e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('Nombre:', name);
        console.log('Mensaje:', message);
        // Puedes enviar los datos a un servidor aquí utilizando fetch o axios
    };

    return (
        <footer className="bg-gray-800 text-white p-8">
            <h2 className='font-BlackOpsOne font-normal uppercase text-4xl text-Viking-500 mx-72'>
                <span className=' text-PictonBlue-300'>&lt;</span>
                &nbsp; Contacteme &nbsp;
                <span className=' text-PictonBlue-300'>&gt;</span>
            </h2>

            <form onSubmit={handleSubmit} className="mx-72 my-5">
                <main className='flex flex-col items-center'>
                    <label className="">
                        <span className="font-BebasNeue text-3xl block">Nombre:</span>
                        <input
                            type="text"
                            value={name}
                            onChange={handleNameChange}
                            className="text-black w-[30rem] my-6 p-2 border border-gray-400 rounded"
                        />
                    </label>
                    <label className="">
                        <span className="font-BebasNeue text-3xl block">Mensaje:</span>
                        <textarea
                            value={message}
                            onChange={handleMessageChange}
                            className="text-black w-[30rem] my-6 p-2 border border-gray-400 rounded"
                        />
                    </label>
                </main>
                <footer className='flex justify-end'>
                    <button
                        type="submit"
                        className="bg-blue-500 text-white p-2 rounded hover:bg-blue-700"
                    >
                        Enviar
                    </button>
                </footer>

            </form>
        </footer>
    );
};

export default Footer;
